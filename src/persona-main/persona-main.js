
import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}

        };
    }

    constructor() {
        super();
        this.people = [
            {
                name: "Eleven",
                yearsInCompany: 12,
                profile: "Lorem ipsum dolor sit amet. Eleven",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Julia Solar"
                }

            }, {
                name: "Mike",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet. Mike",
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "Emma Solar"
                }
            }, {
                name: "Max",
                yearsInCompany: 20,
                profile: "Lorem ipsum dolor sit amet. Max",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Emma Solar"
                }
            }, {
                name: "Steve",
                yearsInCompany: 50,
                profile: "Lorem ipsum dolor sit amet. Steve",
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "Emma Solar"
                }
            }, {
                name: "Will",
                yearsInCompany: 47,
                profile: "Lorem ipsum dolor sit amet.Will",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "JM Solar"
                }
            
            }
        ];

        this.showPersonForm = false;
    }
//fname es de persona-ficha-listado. Name es el nombre que le hemos dado a los componentes Person del array people
// aqui mapeamos el people al html de persona-ficha-listado, dandole valor a cada registro del arraay (person)
// el photo tienen un punto (.) delante porque lo que pinta no es un atributo sino una propiedad. No debe escribir un string de un objeto.
//esto siempre para objetos, el punto delante
    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4" >
                ${this.people.map(
                    person => html`
                    <persona-ficha-listado 
                        fname="${person.name}"
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`
                )}                
                </div>
            </div>
            <div class="row">
                <persona-form 
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}" 
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `;
// d-none = display none, para ocultar
    }

    updated(changedProperties) {
        console.log("estamos en updated persona main");
        console.log(changedProperties);

        if (changedProperties.has("showPersonForm")) {
            console.log("se ha cambiado el valor de la propiedad showperson form en persona main");

            // == solo compara valor. O sea, true == "true" o true == 1 lo da por bueno
            // === compara formato y valor, es más seguro
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxha cambaiado la propiedad people en persona manin");

            this.dispatchEvent(new CustomEvent("updated-people", {

                    "detail": {
                        people: this.people
                    }
                }
            ));
        }
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("mostrando formulario de persona")
        
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }    
    
    showPersonList() {
        console.log("showPersonList");
        console.log("mostrando lista de personas")

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        
    }    

    personFormClose() {
        console.log("personFormClose");
        console.log("se ha cerrdado el formulario de la persona");

        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log("se va a guardar una persona");
        console.log(e.detail);
        console.log("La propiedad name en person en main vale " + e.detail.person.name);
        console.log("La propiedad profile en person en main vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany en person en main vale " + e.detail.person.yearsInCompany);
        // Ponemos e.detail porque e es lo que se captura, y detail es como viene definido en persona-form, que es desde donde se captura
        // push añade registro al array
        
        console.log("la propiedad editing person vale " + e.detail.editingPerson);

        if (e.detail.editingPerson === true) {
            console.log("aqui se va a actualizar la persona " + e.detail.person.name);

            // Si lo encuentra, devuelve el indice en el array (>0).Si no lo ha encontrado devuelve -1
            
            this.people = this.people.map(
                person => person.name === e.detail.person.name 
                            ? person = e.detail.person : person
            );
            // el ? y : en la sentencia de arriba actuan como un IF y else de ese ===
            //Si es true lo de la izquierda de la ?, coge lo de la derecha, sino lo que esta despues de los :

            /*let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            if (indexOfPerson >= 0) {
                console.log("persona encontrada!!!!");
                this.people[indexOfPerson] = e.detail.person;
            }*/          
           
        } else {
            console.log(" se va a almacenar a new persona");
        //    this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person];
            // los ... es como descomponer todo el array, asi se añade facilmente uno nuevo al array original
        }
        

        console.log("persona almacenada");

        this.showPersonForm = false;
    }

    deletePerson(e){
        console.log("delete persone en persona-main");
        console.log("se va a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );

    }
    infoPerson(e){
        console.log("informacion de persona");
        console.log("Se ha pedido desde persona main la informacion de " + e.detail.name);    

        //Pasos: 1. recuperar persona. 2. pasar persona a formulario. 3. mostrar el formulario
        // 1 recuperar persona
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson);
        // 2 pasar persona a formulario. Ojo, que chosenPerson aqui es un array con un object, ojo.
        // Inlcuimos en el person form el .value (value como atributo) en el render html de person form

        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;
        
        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        
        // 3 Mostrar el formulario
        this.showPersonForm = true;
    }
}


//<div class="row" "row-cols-1 row-cols-sm-4" > de SM y los mayores, son 4 columnas. Si es más pequeño ( o sea xs movil), entonces que lo transforme en 1 columna
customElements.define('persona-main', PersonaMain)
