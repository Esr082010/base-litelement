
import {LitElement, html} from 'lit-element';

class PersonaForm extends LitElement{

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}

        };
    }

    constructor() {
        super();

        // inicializamos el objeto en el constructor con {}
        // this.person = {};
        this.resetFormData();
        this.editingPerson = false;
    }

    // ? delante es para tratar booleanos, para una propiedad que es de visibilidad en la ventana
    // . delante es para tratar propiedades y no valores
    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" 
                            @input="${this.updateName}" 
                            .value="${this.person.name}" 
                            ?disabled="${this.editingPerson}"
                            class="form-control" 
                            placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea 
                            @input="${this.updateProfile}" 
                            .value="${this.person.profile}" 
                            class="form-control" 
                            placeholder="Perfil" 
                            rows="5"></textarea
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" 
                            @input="${this.updateYearsInCompany}" 
                            .value="${this.person.yearsInCompany}" 
                            class="form-control" 
                            placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;

    }
    updateName(e) {
        console.log("updatename");
        console.log("actualizando propiedad name " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("actualizando propiedad profile " + e.target.value);
        this.person.profile = e.target.value;
        
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        console.log("actualizando propiedad years " + e.target.value);
        this.person.yearsInCompany = e.target.value;
        
    }

    goBack(e) {
        console.log("goback");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src" : "./img/personaAlta.jpg",
            "alt" : "Persona"
        };

        console.log("La propiedad name en person vale " + this.person.name);
        console.log("La propiedad profile en person vale " + this.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + this.person.yearsInCompany);

        this.dispatchEvent(new CustomEvent("persona-form-store", {
                detail: {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson: this.editingPerson
                }
            })
        );
            
    }
    // creamos funcion de reseteo de datos para el boton atras y en el constructor de persona Form, que equivale
    // al Más adel sidebar, para que no arrastre valores de consultas previas
    resetFormData() {
        console.log("resetFOrmData");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm)
