
import {LitElement, html} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement{

    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;

    }
    processEvent(e){
        console.log("processEvent");
        console.log(e);

        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
        //izquierda del = es la propiedad del receptor evento. Lo de la derecha es lo del emisor event
        //se refiere a la raiz del dom local
    }
}

customElements.define('gestor-evento', GestorEvento)
