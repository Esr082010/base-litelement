
import {LitElement, html} from 'lit-element';

class TestApi extends LitElement{

    static get properties() {
        return {
            movies: {type: Array}
        }
    }

constructor (){
    super();

    // inicializamos movies
    this.movies = [];
    // definimos funcion que se traiga los datos de inicio
    this.getMovieData();

}
    // aqui preparamos la plantilla
    render() {
        return html`
            ${this.movies.map(
                movie => html`<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;
    }

    // y aqui definimos la funcion en detalle
    getMovieData() {
        console.log("getMovieData");
        console.log("obteniendo datos de las peliculas");

        //Usamos AJAX : Asyncronous Java Sript And Xml. Se usa para traer trozos de las paginas
        //AJAX. Pero se sentencia con lo siguiente:
        let xhr = new XMLHttpRequest();
        // xhr tiene la propiedad onload que se va a llamar cuando haya completado la peticion y el servidor ya ha contestado, y asi manejamos la respuesta 
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Peticion completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText);

                // console.log (APIResponse);

                this.movies = APIResponse.results;
                // esta es la linea que más va a fallar en Hackaton...revisar la estructura de lo que devuelvan. Ver si tiene, por ejemplo
                // el results del formato de respuesta de la API. 
            }
        };
        //el open de xhr es como un arranque o inicio  del objeto xhr, pero no hace la peticion
        xhr.open("GET", "https://swapi.dev/api/films/");
        // El send es el que hace la peticion
        xhr.send();
        console.log("fin de getMovieData");
        
    }

} 

customElements.define('test-api', TestApi)
